import { createContext, Dispatch, ReactNode, SetStateAction, useCallback, useContext, useEffect, useState } from 'react'
import { useLevel } from './Level'

export const JET_SITE_ID = 'jet-site'

interface JetSite {
    pathOfVisiblePage: string,
    setPathOfVisiblePage: Dispatch<SetStateAction<string>>,
    pathOfPageToFix: string,
    contentOfPageToFix: string,
    setUserProvidedContent: Dispatch<SetStateAction<Record<string, string>>>,
    iFrame: HTMLIFrameElement | undefined
}

const JetSiteContext = createContext<JetSite>({
    pathOfVisiblePage: '',
    setPathOfVisiblePage: () => { },
    pathOfPageToFix: '',
    contentOfPageToFix: '',
    setUserProvidedContent: () => { },
    iFrame: undefined
})

const DEFAULT_CONTENT = import.meta.glob('../jet-site-content/*.html', {
    as: 'raw',
    eager: true
})

export function JetSiteProvider({ children }: { children: (JetSiteIframe: () => JSX.Element) => ReactNode }) {
    const { pathOfPageToFix } = useLevel()
    const [pathOfVisiblePage, setPathOfVisiblePage] = useState(pathOfPageToFix)
    const [userProvidedContent, setUserProvidedContent] = useState<Record<string, string>>({})
    const [iFrame, setIFrame] = useState<HTMLIFrameElement>()

    const JetSiteIFrame = useCallback(function () {

        function onIFramePageLoaded(iFrame: HTMLIFrameElement): void {
            setIFrame(iFrame)
            const newPath = extractIFramePath(iFrame)
            if (isInBounds(newPath)) {
                setPathOfVisiblePage(newPath)
            } else {
                redirectBackToPreviousPage()
            }
        }

        function isInBounds(path: string) {
            if (path === '/out-of-bounds') {
                return false
            }
            return !!lookupContent(path)
        }

        function redirectBackToPreviousPage() {
            setPathOfVisiblePage(previousPath => {
                // There's a bug here.
                // When you navigate to the next page, and then navigate out of bounds,
                // and then use the editor button to return, then this callback will trigger bringing you to the wrong page.
                setTimeout(() => setPathOfVisiblePage(previousPath), 3000)
                return '/out-of-bounds'
            })
        }

        return <iframe id={JET_SITE_ID}
            src={`/iframe${pathOfVisiblePage}`}
            title="a11yJet, a website with deliberate accessibility flaws"
            className="h-full max-h-[680px] border-y-[50px] border-x-[15px] border-zinc-900 rounded-[20px] my-auto p-0"
            onLoad={(event) => onIFramePageLoaded(event.currentTarget)}
        />
    }, [])

    const contentOfVisiblePage = lookupContent(pathOfVisiblePage)
    const contentOfPageToFix = lookupContent(pathOfPageToFix)

    function lookupContent(path: string) {
        return userProvidedContent[path] || DEFAULT_CONTENT[`../jet-site-content${path}.html`]
    }

    useEffect(function passContentToIFrame() {
        if (!iFrame?.contentDocument) {
            return
        }
        // Matches ID in iFrame.html.
        const contentReciever = iFrame.contentDocument.getElementById('content-receiver')
        if (!contentReciever) {
            console.error('Trying to pass content to iFrame, but receiving element is not present.')
            return
        }
        contentReciever.innerHTML = pathOfVisiblePage === pathOfPageToFix
            ? contentOfPageToFix
            : contentOfVisiblePage
    }, [iFrame?.contentDocument, pathOfVisiblePage, pathOfPageToFix, contentOfPageToFix, contentOfVisiblePage])

    return <JetSiteContext.Provider value={{
        pathOfVisiblePage, setPathOfVisiblePage,
        pathOfPageToFix,
        contentOfPageToFix, setUserProvidedContent,
        iFrame
    }}>
        {children(JetSiteIFrame)}
    </JetSiteContext.Provider>
}

export function useJetSiteIFrame() {
    const { iFrame } = useContext(JetSiteContext)
    return iFrame
}

export function useJetSiteNavigation() {
    const { pathOfPageToFix, pathOfVisiblePage, setPathOfVisiblePage } = useContext(JetSiteContext)

    return {
        pageToFixIsVisiblePage: pathOfPageToFix === pathOfVisiblePage,
        navigateToPageToFix: () => {
            setPathOfVisiblePage(pathOfPageToFix)
        }
    }
}

export function useJetSiteContent() {
    const { contentOfPageToFix, setUserProvidedContent, pathOfPageToFix } = useContext(JetSiteContext)

    return {
        contentOfPageToFix,
        setContent: (newContent: string) => {
            setUserProvidedContent(existingContent => ({
                ...existingContent,
                [pathOfPageToFix]: newContent
            }))
        },
        resetContent: () => {
            setUserProvidedContent(existingContent => {
                const { [pathOfPageToFix]: _removed, ...remainingContent } = existingContent
                return remainingContent
            })
        }
    }
}

function extractIFramePath(iFrame: HTMLIFrameElement) {
    return new URL(iFrame.contentDocument!.URL).pathname.replace('/iframe', '')
}