import { useCallback, useEffect } from 'react';
import { useParams } from 'react-router';
import { useLocation, useNavigate } from 'react-router-dom';
import { levelFourTestSteps } from '../test-steps/LevelFourTestSteps';
import { levelOneTestSteps } from '../test-steps/LevelOneTestSteps';
import { levelThreeTestSteps } from '../test-steps/LevelThreeTestSteps';
import { levelTwoTestSteps } from '../test-steps/LevelTwoTestSteps';
import { levelSixTestSteps } from '../test-steps/LevelSixTestSteps';
import { levelFiveTestSteps } from '../test-steps/LevelFiveTestSteps';

export interface Step<Result, Input> {
    descriptionOfStep: string,
    descriptionOfSuccess: string,
    runStep: (jetSiteDocument: Document, input: Input) => Promise<Result>
}

interface Level<Result, Input> {
    title: string
    pathOfPageToFix: string
    testSteps: Step<Result, Input>[]
    hints: string[]
}

const LEVELS: Record<number, Level<any, any>> = {
    1: {
        title: 'Alter ego',
        pathOfPageToFix: '/index',
        testSteps: levelOneTestSteps,
        hints: [
            '🤔 How does a screen reader read the offer image?',
            'Images should have a non-empty "alt" tag, describing their content'
        ]
    },
    2: {
        title: 'Read the label carefully',
        pathOfPageToFix: '/input-labels',
        testSteps: levelTwoTestSteps,
        hints: [
            '🤔 What does a screen reader know about the inputs?',
            'The label element can be used to indicate the purpose of an input'
        ]
    },
    3: {
        title: 'Describe in detail',
        pathOfPageToFix: '/form-validation',
        testSteps: levelThreeTestSteps,
        hints: [
            'Do users know which inputs are invalid?',
            'Do users know why the inputs are considered invalid?'
        ]
    },
    4: {
        title: 'Compare and ...',
        pathOfPageToFix: '/contrast',
        testSteps: levelFourTestSteps,
        hints: [
            'Text should have a contrast ratio of at least 4.5:1, or 3:1 for large text (18pt)',
            'Your browser\'s developer tools can be used to check contrast ratios'
        ]
    },
    5: {
        title: 'Skipping the queue',
        pathOfPageToFix: '/skip-to-main-content',
        testSteps: levelFiveTestSteps,
        hints: [
            'It\'s good practice to add a "Skip to content" link to your website',
            'A "Skip to content" link allows users of screen readers and keyboards to avoid repetative content like navigation',
            '"Skip to content" links are typically only visible when focused',
            'CSS frameworks, like Bootstrap and Tailwind, often have utility classes to help with visibility such as "sr-only" and "focus:not-sr-only"',
            'For an element to be a target of a link it must be focusable, this can be achieved by adding tabIndex="-1"'
        ]
    },
    6: {
        title: 'Heading to the top',
        pathOfPageToFix: '/structure',
        testSteps: levelSixTestSteps,
        hints: [
            'Headings provide structure to a page, they let users of screen readers skim through content',
            'Using headings with different levels provides even more structure'
        ]
    }
}

export function useLevel() {

    const params = useParams()
    const location = useLocation()
    const navigate = useNavigate()
    const level = params.level === undefined ? undefined : parseInt(params.level)

    const reachedEndOfChallenge = level && !LEVELS[level]
    const levelComplete = location.state?.complete

    const setLevelComplete = useCallback(() => {
        navigate('', {
            state: {
                complete: true
            }
        })
    }, [navigate])

    useEffect(() => {
        if (reachedEndOfChallenge) {
            navigate('/end-of-challenge')
        }
    }, [reachedEndOfChallenge])

    return {
        level,
        levelComplete,
        setLevelComplete,
        ...(
            level === undefined || reachedEndOfChallenge
                ? {
                    level: undefined,
                    title: undefined,
                    pathOfPageToFix: '/index',
                    testSteps: [],
                    hints: []
                }
                : LEVELS[level]
        )
    }
}
