import { findByText, getAllByRole, queryByText, waitFor } from '@testing-library/dom'

const lookForSkipToContentLink = {
    descriptionOfStep: 'Looking for "Skip to content" link',
    descriptionOfSuccess: 'Found "Skip to content" link',
    runStep: async ({ body }: Document) => {
        // I tried using `userEvent.tab` here, to see if the first focused element was a skip link.
        // But `userEvent.tab` wouldn't change the focus of the document.
        // Instead we have to guess what text the developer might use.
        const links = getAllByRole(body, 'link')
        const skipToContentLink = links.find(l => l.textContent?.includes('Skip to content') || l.textContent?.includes('Skip to main content'))
        if (skipToContentLink) {
            return skipToContentLink
        }
        throw new Error('Could not find "Skip to content" link')
    }
}

const checkVisibilityOfSkipToContentLink = {
    descriptionOfStep: 'Checking "Skip to content" link is only visible when focused',
    descriptionOfSuccess: 'The "Skip to content" link was only visible when focused',
    runStep: async (_: Document, skipToContentLink: HTMLLinkElement) => {
        if (await elementIsVisible(skipToContentLink)) {
            throw new Error('The "Skip to content" link should not be visible by default')
        }
        skipToContentLink.focus()
        if (!await elementIsVisible(skipToContentLink)) {
            throw new Error('The "Skip to content" link should be visible when focused')
        }
        return skipToContentLink
    }
}

const clickSkipToContentLink = {
    descriptionOfStep: 'Clicking "Skip to content" link',
    descriptionOfSuccess: 'Clicked "Skip to content" link',
    runStep: async (_: Document, skipToContentLink: HTMLLinkElement) => {
        skipToContentLink.click()
    }
}

const checkThatMainContentIsFocused = {
    descriptionOfStep: 'Checking that main content is focused',
    descriptionOfSuccess: 'Main content was focused',
    runStep: async (document: Document) => {
        if (document.activeElement?.tagName !== 'MAIN') {
            throw new Error('Expected "main" element to be focused')
        }
    }
}

function elementIsVisible(element: HTMLElement) {
    return new Promise(resolve => {
        const observer = new IntersectionObserver(([entry]) => {
            resolve(entry.intersectionRatio === 1)
            observer.disconnect()
        });
        observer.observe(element)
    })
}
const navigateToTermsAndConditionsPage = {
    descriptionOfStep: 'Click "Terms and conditions" link',
    descriptionOfSuccess: 'Clicked "Terms and conditions" link',
    runStep: async ({ body }: Document) => {
        const flight = queryByText(body, 'Terms and conditions', { exact: false })
        if (!flight) {
            throw new Error('Could not find "Terms and conditions" link')
        }
        flight.click()
    }
}

const verifyNavigatedToTermsAndConditionsPage = {
    descriptionOfStep: 'Looking for content from "Terms and conditions" page',
    descriptionOfSuccess: 'Found content from "Terms and conditions" page',
    // Avoid destructuring document immediately, page may not have changed when step is run.
    runStep: async (document: Document) => {
        await waitFor(() => {
            const contentFromPage = queryByText(
                document.body,
                'Your rights'
            )
            if (!contentFromPage) {
                throw new Error('Could not find content from "Terms and conditions" page')
            }
        })
    }
}

export const levelFiveTestSteps = [
    lookForSkipToContentLink,
    checkVisibilityOfSkipToContentLink,
    clickSkipToContentLink,
    checkThatMainContentIsFocused,
    navigateToTermsAndConditionsPage,
    verifyNavigatedToTermsAndConditionsPage
]
