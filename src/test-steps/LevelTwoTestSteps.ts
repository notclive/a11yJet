import { getByRole, getByText, queryByLabelText, queryByText, waitFor } from '@testing-library/dom'
import userEvent from '@testing-library/user-event'

const findOriginLabel = {
    descriptionOfStep: 'Looking for "Where are you flying from?" label',
    descriptionOfSuccess: 'Found "Where are you flying from?" label',
    runStep: async ({ body }: Document) => {
        const byLabel = queryByLabelText(body, 'Where are you flying from?', { exact: false })
        if (byLabel) {
            return
        }

        const possibleLabel = getByText(body, 'Where are you flying from?')
        if (possibleLabel?.tagName === 'LABEL') {
            return
        }

        throw new Error('Could not find "Where are you flying from?" label')
    }
}

const checkAssociation = {
    descriptionOfStep: 'Check that label was associated with dropdown',
    descriptionOfSuccess: 'Label was associated with dropdown',
    runStep: async ({ body }: Document) => {
        const dropdown = queryByLabelText(body, 'Where are you flying from?', { exact: false })
        if (dropdown?.tagName === 'SELECT') {
            return dropdown
        }
        throw new Error('Expected label to be associated with dropdown')
    }
}

const selectDestination = {
    descriptionOfStep: 'Selecting "[MAN] Manchester" as a destination as a destination',
    descriptionOfSuccess: 'Selected "[MAN] Manchester" as a destination as a destination',
    runStep: async ({ body }: Document, destinationDropdown: HTMLSelectElement) => {
        const manchesterOption = getByRole(body, 'option', { name: '[MAN] Manchester' }) as HTMLOptionElement
        userEvent.selectOptions(destinationDropdown, manchesterOption)
        if (destinationDropdown.selectedOptions.length !== 1 &&
            destinationDropdown.selectedOptions[0].value !== '[MAN] Manchester'
        ) {
            throw new Error('Could not select "[MAN] Manchester" as a destination')
        }
    }
}

const findDepartureLabel = {
    descriptionOfStep: 'Looking for "Departure date" label',
    descriptionOfSuccess: 'Found "Departure date" label',
    runStep: async ({ body }: Document) => {
        const byLabel = queryByLabelText(body, 'Departure date', { exact: false })
        if (byLabel) {
            return byLabel
        }
        throw new Error('Could not find "Departure date" label')
    }
}

const fillInDepartureDate = {
    descriptionOfStep: 'Filling in departure date with past date',
    descriptionOfSuccess: 'Filled in departure date with past date',
    runStep: async (_document: Document, dateInput: HTMLInputElement) => {
        userEvent.clear(dateInput)
        userEvent.type(dateInput, '01/03/2023')
    }
}

const findReturnLabel = {
    descriptionOfStep: 'Looking for "Return date" label',
    descriptionOfSuccess: 'Found "Return date" label',
    runStep: async ({ body }: Document) => {
        const byLabel = queryByLabelText(body, 'Return date', { exact: false })
        if (byLabel) {
            return byLabel
        }
        throw new Error('Could not find "Return date" label')
    }
}

const fillInReturnDate = {
    descriptionOfStep: 'Filling in return date with American style date',
    descriptionOfSuccess: 'Filled in return date with American style date',
    runStep: async (_document: Document, dateInput: HTMLInputElement) => {
        userEvent.clear(dateInput)
        userEvent.type(dateInput, '10/30/2023')
    }
}

const submitForm = {
    descriptionOfStep: 'Submitting form',
    descriptionOfSuccess: 'Submitted form',
    runStep: async ({ body }: Document) => {
        const submitButton = queryByText(
            body,
            'Find flights',
            {
                selector: 'button'
            }
        )
        if (!submitButton) {
            throw new Error('Could not find "Find flights" button')
        }
        submitButton.click()
    }
}

const verifyNavigatedToFormValidationPage = {
    descriptionOfStep: 'Looking for validation message to indicate form was submitted',
    descriptionOfSuccess: 'Found validation message to indicate form was submitted',
    // Avoid destructuring document immediately, page may not have changed when test is run.
    runStep: async (document: Document) => {
        await waitFor(() => {
            const validationMessage = queryByText(
                document.body,
                'There were problems with the values you provided',
                {
                    exact: false
                }
            )
            if (!validationMessage) {
                throw new Error('Could not find validation message to indicate form was submitted')
            }
        })
    }
}

export const levelTwoTestSteps = [
    findOriginLabel,
    checkAssociation,
    selectDestination,
    findDepartureLabel,
    fillInDepartureDate,
    findReturnLabel,
    fillInReturnDate,
    submitForm,
    verifyNavigatedToFormValidationPage
]
