import { queryByAltText, queryByText, waitFor } from '@testing-library/dom'

const findPromotion = {
    descriptionOfStep: 'Looking for "Great Caribbean offers" promotion',
    descriptionOfSuccess: 'Found "Great Caribbean offers" promotion',
    runStep: async ({ body }: Document) => {
        const byAltText = queryByAltText(body, 'Great Caribbean offers')
        if (byAltText) {
            return byAltText
        }
        const byText = queryByText(body, 'Great Caribbean offers')
        if (byText) {
            return byText
        }
        throw new Error('Could not find "Great Caribbean offers" promotion')
    }
}

const clickPromotion = {
    descriptionOfStep: 'Clicking promotion',
    descriptionOfSuccess: 'Clicked promotion',
    runStep: async (_document: Document, promotion: HTMLElement) => {
        promotion.click()
    }
}

const verifyNavigatedToCaribbeanOffersPage = {
    descriptionOfStep: 'Looking for content from "Caribbean offers" page',
    descriptionOfSuccess: 'Found content from "Caribbean offers" page',
    // Avoid destructuring document immediately, page may not have changed when step is run.
    runStep: async (document: Document) => {
        await waitFor(() => {
            const contentFromPage = queryByText(
                document.body,
                'Find great value flights to the Caribbean'
            )
            if (!contentFromPage) {
                throw new Error('Could not find content from "Caribbean offers" page')
            }
        })
    }
}

export const levelOneTestSteps = [
    findPromotion,
    clickPromotion,
    verifyNavigatedToCaribbeanOffersPage
]
