import axe from 'axe-core'
import { JET_SITE_ID } from '../state/JetSite'
import { queryByText, waitFor } from '@testing-library/dom'

const checkContrast = {
    descriptionOfStep: 'Checking text contrast',
    descriptionOfSuccess: 'Text has sufficient contrast',
    runStep: async ({ body }: Document) => {
        const results = await axe.run(
            window.document.getElementById(JET_SITE_ID)!,
            {
                runOnly: {
                    type: 'rule',
                    values: ['color-contrast']
                }
            }
        )
        if (results.violations.length > 0) {
            throw new Error(`Found text with insufficient contrast: ${results.violations[0].nodes[0].html}`)
        }
    }
}

const selectFlight = {
    descriptionOfStep: 'Selecting flight',
    descriptionOfSuccess: 'Selected flight',
    runStep: async ({ body }: Document) => {
        const flight = queryByText(body, 'Manchester to Barbados', { exact: false })
        if (!flight) {
            throw new Error('Could not find "Manchester to Barbados" flight')
        }
        flight.click()
    }
}

const verifyNavigatedToConfirmationPage = {
    descriptionOfStep: 'Looking for content from "Confirmation" page',
    descriptionOfSuccess: 'Found content from "Confirmation" page',
    // Avoid destructuring document immediately, page may not have changed when step is run.
    runStep: async (document: Document) => {
        await waitFor(() => {    
            const contentFromPage = queryByText(
                document.body,
                'Your flight is confirmed'
            )
            if (!contentFromPage) {
                throw new Error('Could not find content from "Confirmation" page')
            }
        })
    }
}

export const levelFourTestSteps = [
    checkContrast,
    selectFlight,
    verifyNavigatedToConfirmationPage
]
