import { queryByLabelText, queryByText, waitFor } from '@testing-library/dom'

const findDepartureLabel = {
    descriptionOfStep: 'Looking for "Departure date" label',
    descriptionOfSuccess: 'Found "Departure date" label',
    runStep: async ({ body }: Document) => {
        const byLabel = queryByLabelText(body, 'Departure date', { exact: false })
        if (byLabel) {
            return byLabel
        }
        throw new Error('Could not find "Departure date" label')
    }
}

const verifyThatDepartureDateIsMarkedAsInvalid = {
    descriptionOfStep: 'Verifying that "Departure date" input is marked as invalid',
    descriptionOfSuccess: 'Verified that "Departure date" input is marked as invalid',
    runStep: async (_: Document, departureInput: HTMLInputElement) => {
        if (departureInput.getAttribute('aria-invalid')) {
            return departureInput
        }
        throw new Error('"Departure date" input was not marked as invalid')
    }
}

const findMessageThatDescribesDepartureDateError = {
    descriptionOfStep: 'Looking for message that describes why "Departure date" is invalid',
    descriptionOfSuccess: 'Found message that describes why "Departure date" is invalid',
    runStep: async (document: Document, departureInput: HTMLInputElement) => {
        const describedById = departureInput.getAttribute("aria-describedBy")
        if (!describedById) {
            throw new Error('Could not find "aria-describedBy" attribute on "Departure date" input')
        }
        const errorMessage = document.getElementById(describedById)
        if (errorMessage) {
            return errorMessage
        }
        throw new Error(`Could not find element with ID "${describedById}"`)
    }
}

const verifyDepartureDateErrorMessage = {
    descriptionOfStep: 'Verifying that error message for "Departure date" mentions "past"',
    descriptionOfSuccess: 'The error message for the "Departure date" mentions "past"',
    runStep: async ({ }: Document, errorMessage: HTMLElement) => {
        if (!errorMessage.textContent?.includes('past')) {
            throw new Error('The error message for the "Departure date" does not mention "past"')
        }
    }
}

const findReturnLabel = {
    descriptionOfStep: 'Looking for "Return date" label',
    descriptionOfSuccess: 'Found "Return date" label',
    runStep: async ({ body }: Document) => {
        const byLabel = queryByLabelText(body, 'Return date', { exact: false })
        if (byLabel) {
            return byLabel
        }
        throw new Error('Could not find "Return date" label')
    }
}

const verifyThatReturnDateIsMarkedAsInvalid = {
    descriptionOfStep: 'Verifying that "Return date" input is marked as invalid',
    descriptionOfSuccess: 'Verified that "Return date" input is marked as invalid',
    runStep: async ({ }: Document, returnInput: HTMLInputElement) => {
        if (returnInput.getAttribute('aria-invalid')) {
            return returnInput
        }
        throw new Error('"Return date" input was not marked as invalid')
    }
}

const findMessageThatDescribesReturnDateError = {
    descriptionOfStep: 'Looking for message that describes why "Departure date" is invalid',
    descriptionOfSuccess: 'Found message that describes why "Departure date" is invalid',
    runStep: async (document: Document, returnInput: HTMLInputElement) => {
        const describedById = returnInput.getAttribute("aria-describedBy")
        if (!describedById) {
            throw new Error('Could not find "aria-describedBy" attribute on "Departure date" input')
        }
        const errorMessage = document.getElementById(describedById)
        if (errorMessage) {
            return errorMessage
        }
        throw new Error(`Could not find element with ID "${describedById}"`)
    }
}

const verifyReturnDateErrorMessage = {
    descriptionOfStep: 'Verifying that error message for "Return date" mentions "format"',
    descriptionOfSuccess: 'The error message for the "Return date" mentions "format"',
    runStep: async ({ }: Document, errorMessage: HTMLElement) => {
        if (!errorMessage.textContent?.includes('format')) {
            throw new Error('The error message for the "Return date" does not mention "format"')
        }
    }
}

const submitForm = {
    descriptionOfStep: 'Submitting form',
    descriptionOfSuccess: 'Submitted form',
    runStep: async ({ body }: Document) => {
        const submitButton = queryByText(
            body,
            'Find flights',
            {
                selector: 'button'
            }
        )
        if (!submitButton) {
            throw new Error('Could not find "Find flights" button')
        }
        submitButton.click()
    }
}

const verifyNavigatedToFlightSelectionPage = {
    descriptionOfStep: 'Looking for content from "Flight selection" page',
    descriptionOfSuccess: 'Found content from "Flight selection" page',
    // Avoid destructuring document immediately, page may not have changed when step is run.
    runStep: async (document: Document) => {
        await waitFor(() => {
            const contentFromPage = queryByText(
                document.body,
                'Select a flight'
            )
            if (!contentFromPage) {
                throw new Error('Could not find content from "Flight selection" page')
            }
        })
    }
}

export const levelThreeTestSteps = [
    findDepartureLabel,
    verifyThatDepartureDateIsMarkedAsInvalid,
    findMessageThatDescribesDepartureDateError,
    verifyDepartureDateErrorMessage,
    findReturnLabel,
    verifyThatReturnDateIsMarkedAsInvalid,
    findMessageThatDescribesReturnDateError,
    verifyReturnDateErrorMessage,
    submitForm,
    verifyNavigatedToFlightSelectionPage
]
