import { getAllByRole } from '@testing-library/dom'

const checkForRefundPolicyHeading = {
    descriptionOfStep: 'Looking for heading to "Refund policy" section',
    descriptionOfSuccess: 'Found heading for "Refund policy" section',
    runStep: async ({ body }: Document) => {
        const headings = getAllByRole(body, 'heading')
        const refundPolicyHeading = headings.find(h => h.textContent?.includes('Refund policy'))
        if (!refundPolicyHeading) {
            throw new Error(`Could not find heading for "Refund policy" section`)
        }
    }
}

const checkForSecurityRulesHeading = {
    descriptionOfStep: 'Looking for heading to "Security rules" section',
    descriptionOfSuccess: 'Found heading for "Security rules" section',
    runStep: async ({ body }: Document) => {
        const headings = getAllByRole(body, 'heading')
        const refundPolicyHeading = headings.find(h => h.textContent?.includes('Security rules'))
        if (!refundPolicyHeading) {
            throw new Error(`Could not find heading for "Security rules" section`)
        }
    }
}

const checkThatHeadingsAreHierarchical = {
    descriptionOfStep: 'Verifying that "Prohibited luggage" is nested beneath "Rules of travel" heading',
    descriptionOfSuccess: 'The "Prohibited luggage" heading was beneath "Rules of travel" heading',
    runStep: async ({ body }: Document) => {
        const headings = getAllByRole(body, 'heading')
        const rulesOfTravelHeading = headings.find(h => h.textContent?.includes('Rules of travel'))
        if (!rulesOfTravelHeading) {
            throw new Error(`Could not find heading for "Rules of travel" section`)
        }
        const prohibitedLuggageHeading = headings.find(h => h.textContent?.includes('Prohibited luggage'))
        if (!prohibitedLuggageHeading) {
            throw new Error(`Could not find heading for "Prohibited luggage" section`)
        }

        const rulesOfTravelLevel = determineHeadingLevel(rulesOfTravelHeading)
        const prohibitedLuggageLevel = determineHeadingLevel(prohibitedLuggageHeading)
        if (prohibitedLuggageLevel !== rulesOfTravelLevel + 1) {
            throw new Error(`The "Prohibited luggage" heading should be level ${rulesOfTravelLevel + 1}`)
        }
    }
}

function determineHeadingLevel(heading: HTMLElement) {
    if (heading.tagName.startsWith('H')) {
        return parseInt(heading.tagName.replace('H', ''))
    }
    const ariaLevel = parseInt(heading.getAttribute('aria-level')!)
    if (isNaN(ariaLevel)) {
        throw new Error(`Heading "${heading.innerText}" must define a level using aria-level`)
    }
    return ariaLevel
}

export const levelSixTestSteps = [
    checkForRefundPolicyHeading,
    checkForSecurityRulesHeading,
    checkThatHeadingsAreHierarchical
]
