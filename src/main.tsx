import React from 'react'
import ReactDOM from 'react-dom/client'
import { createBrowserRouter, RouterProvider } from 'react-router-dom'
import App from './App'
import { EndOfChallengePage } from './pages/end-of-challenge-page/EndOfChallengePage'
import { SolveLevelPage } from './pages/solve-level-page/SolveLevelPage'
import { UnderstandChallengePage } from './pages/understand-challenge-page/UnderstandChallengePage'
import { initialiseAnalytics } from './analytics'

initialiseAnalytics()

const router = createBrowserRouter([{
  path: '*',
  element: <App />,
  children: [{
    path: '*',
    element: <UnderstandChallengePage />
  }, {
    path: 'level/:level',
    element: <SolveLevelPage />
  }, {
    path: 'end-of-challenge',
    element: <EndOfChallengePage />
  }]
}])

ReactDOM.createRoot(document.getElementById('root') as HTMLElement).render(
  <React.StrictMode>
    <RouterProvider router={router} />
  </React.StrictMode>,
)
