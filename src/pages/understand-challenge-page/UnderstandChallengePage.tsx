import { Link } from 'react-router-dom'

export function UnderstandChallengePage() {
    return <div className='flex-1 flex flex-col justify-center items-center'>
        <h2 className="my-4 text-3xl">What is a11yJet?</h2>
        <p className="mb-8 text-center">
            <span title="a11y is short for accessibility">a11yJet</span> is a fictitious airline whose website isn't very accessible<br />
            Your challenge is to find and fix the accessibility flaws in the website
        </p>
        <Link to="/level/1"
              className="bg-green-700 hover:bg-green-800 text-white font-bold py-2 px-4 rounded"
        >
            Show me the source code
        </Link>
    </div>
}