export function EndOfChallengePage() {

    return <div className="h-full flex justify-center items-center flex-col">
            <h2 className="text-3xl mb-4">
                You've reached the end of the challenge
            </h2>
            <p>
                To contribute your own levels, see <a href="https://gitlab.com/notclive/a11yJet" className="text-teal-900 underline">the project on GitLab</a>.
            </p>
    </div>
}