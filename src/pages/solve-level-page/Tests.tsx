import { useEffect, useState } from 'react'
import { useJetSiteContent, useJetSiteIFrame, useJetSiteNavigation } from '../../state/JetSite'
import { Step, useLevel } from '../../state/Level'
import { waitFor } from '@testing-library/dom'

enum StepResult {
    IN_PROGRESS,
    SUCCESS,
    FAILED
}

interface StepPerformed {
    result: StepResult,
    description: string
}

export function Tests() {

    const [stepsPerformed, setStepsPerformed] = useState<StepPerformed[]>([])
    const { level, levelComplete, setLevelComplete, testSteps, hints } = useLevel()
    const [hintIndex, setHintIndex] = useState<number>()
    const { navigateToPageToFix } = useJetSiteNavigation()
    const { resetContent } = useJetSiteContent()
    const jetSiteIFrame = useJetSiteIFrame()

    useEffect(function resetWhenLevelChanges() {
        setHintIndex(undefined)
        setStepsPerformed([])
    }, [level])

    async function performStep<Result>(
        previousSteps: StepPerformed[],
        step: Step<Result, any>,
        input: any
    ) {
        setStepsPerformed([...previousSteps, {
            result: StepResult.IN_PROGRESS,
            description: step.descriptionOfStep
        }])
        await waitOneSecond()
        try {
            const result = await step.runStep(jetSiteIFrame!.contentDocument!, input)
            previousSteps.push({
                result: StepResult.SUCCESS,
                description: step.descriptionOfSuccess
            })
            setStepsPerformed([...previousSteps])
            return result
        } catch (error) {
            previousSteps.push({
                result: StepResult.FAILED,
                description: error instanceof Error ? error.message : 'Failed due to unexpected error'
            })
            setStepsPerformed([...previousSteps])
            throw new Error('Test failed')
        }
    }

    async function runTests() {
        navigateToPageToFix()
        const stepsPerformed: StepPerformed[] = []
        try {
            let previousResult = null
            for (const step of testSteps) {
                previousResult = await performStep(stepsPerformed, step, previousResult)
            }
            setLevelComplete()
        } catch (error) {
        }
    }

    return <div className="w-full">
        <p className="flex gap-2">
            {!levelComplete && <>
                    <button onClick={resetContent}
                        className="bg-blue-600 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded">
                        Reset HTML
                    </button>
                    {/* Hidding the button until the jet site is loaded is mainly for the benefit of Playwright tests. */}
                    {jetSiteIFrame && <button className="bg-green-700 hover:bg-green-800 text-white font-bold py-2 px-4 rounded"
                        onClick={runTests}>
                        Test my fix
                    </button>}
                </>
            }
        </p>

        <ol className="my-2" aria-live="assertive">
            {/* maxHeight (used for animation) prevents content from being visible if it wraps onto third line. */}
            {testSteps.map((_testStep, index) => {
                return <li style={{ transition: 'all 0.7s', overflow: 'hidden', maxHeight: stepsPerformed.length > index ? 50 : 0 }} key={index}>
                    {stepsPerformed.length > index && stepsPerformed[index].result === StepResult.IN_PROGRESS && `⏳ ${stepsPerformed[index].description}`}
                    {stepsPerformed.length > index && stepsPerformed[index].result === StepResult.SUCCESS && `✔️ ${stepsPerformed[index].description}`}
                    {stepsPerformed.length > index && stepsPerformed[index].result === StepResult.FAILED && `❌ ${stepsPerformed[index].description}`}
                </li>
            })}
        </ol>

        {hints.length > 0 && <div className="relative flex justify-center items-center bg-blue-100 rounded px-2 py-5">
            <div className={`flex gap-1 flex-1 ${hintIndex === undefined ? 'blur' : ''}`}
                aria-hidden={hintIndex === undefined}>
                <span className="flex">
                    {hintIndex !== undefined && hintIndex > 0 &&
                        <button className="px-2"
                            aria-label="Previous hint"
                            onClick={() => setHintIndex(hintIndex - 1)}>
                            ◄
                        </button>
                    }
                </span>
                <span className="flex-1 text-center">
                    {hints[hintIndex ?? 0]}
                </span>
                <span className="flex">
                    {hintIndex !== undefined && hintIndex < hints.length - 1 &&
                        <button className="px-2"
                            aria-label="Next hint"
                            onClick={() => setHintIndex(hintIndex + 1)}>
                            ►
                        </button>
                    }
                </span>
            </div>
            {hintIndex === undefined && <div className="absolute inset-0 flex justify-center items-center">
                <button className="bg-blue-600 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"
                    onClick={() => setHintIndex(0)}>
                    Give me a hint
                </button>
            </div>}
        </div>}
    </div>
}

function waitOneSecond() {
    return new Promise(resolve => setTimeout(resolve, 1000))
}