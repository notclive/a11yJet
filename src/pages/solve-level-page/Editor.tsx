import { Link } from 'react-router-dom'
import { useJetSiteContent, useJetSiteNavigation } from '../../state/JetSite'
import { useLevel } from '../../state/Level'
import { useMemo } from 'react'

enum ButtonToShow {
    NoButton,
    ReturnToPage,
    NextLevel
}

export function Editor() {
    const { level, levelComplete } = useLevel()
    const { contentOfPageToFix, setContent } = useJetSiteContent()
    const { pageToFixIsVisiblePage, navigateToPageToFix } = useJetSiteNavigation()

    const buttonToShow = useMemo(() => {
        if (levelComplete) {
            return ButtonToShow.NextLevel
        }
        if (!pageToFixIsVisiblePage) {
            return ButtonToShow.ReturnToPage
        }
        return ButtonToShow.NoButton
    }, [levelComplete, pageToFixIsVisiblePage])

    return <div className="h-full flex flex-col">
        <div className="flex-1 relative flex border-2 border-zinc-400 rounded">
            <textarea
                aria-label="a11yJet source code"
                className={`flex-1 bg-zinc-50 resize-none p-1 ${buttonToShow !== ButtonToShow.NoButton ? 'blur' : ''}`}
                style={{ boxSizing: 'border-box' }}
                disabled={!pageToFixIsVisiblePage}
                value={contentOfPageToFix}
                onChange={(event) => setContent(event.target.value)}
            />
            {buttonToShow !== ButtonToShow.NoButton && <div className="absolute inset-0 flex justify-center items-center">
                {buttonToShow === ButtonToShow.NextLevel && <Link to={`/level/${level! + 1}`}
                    className="bg-green-700 hover:bg-green-800 text-white font-bold py-2 px-4 rounded"
                >
                    Next level →
                </Link>}
                {buttonToShow === ButtonToShow.ReturnToPage && <button className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"
                    onClick={navigateToPageToFix}>
                    Return to page for current level
                </button>}
            </div>}
        </div>
    </div>
}