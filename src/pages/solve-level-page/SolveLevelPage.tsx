import { Editor } from './Editor';
import { Tests } from './Tests';

export function SolveLevelPage() {
    return <>
        <Editor />
        <Tests />
    </>
}