import { init, trackPages } from 'insights-js'

export function initialiseAnalytics() {
    init('IfOswngpmRLPETTS')
    trackPages()
}