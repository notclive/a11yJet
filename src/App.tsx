import { Outlet } from 'react-router-dom'
import { JetSiteProvider } from './state/JetSite'
import { useLevel } from './state/Level'
import './App.css'

function App() {

  const { level, title } = useLevel()

  return (
    <div className="h-full flex flex-col">
      <header className="bg-zinc-600 text-white py-4 px-8">
        <h1 className="text-2xl max-w-3xl">
          {level
            ? <a href=""><b className="mr-3">Level {level}</b> {title}</a>
            : <a href="/"><b className="mr-3">a11yJet</b> Learn web accessibility</a>}
        </h1>
      </header>
      <div className="flex flex-1 gap-8 align-stretch justify-around p-8">
        <JetSiteProvider>{(JetSiteIFrame) => <>
          <div className="flex flex-1 max-w-3xl flex-col gap-4">
            <Outlet />
          </div>
          <div className="flex flex-col">
            <JetSiteIFrame />
          </div>
        </>
        }
        </JetSiteProvider>
      </div>
      <footer className="text-center bg-zinc-600 text-white p-2">
        Made by <a className="text-blue-200 underline" href="mailto:notclive@protonmail.com">Jonathan Price</a>
      </footer>
    </div >
  )
}

export default App
