# a11yJet

## Getting started

1. The project relies on `corepack` to select the appropriate version of `yarn`, to enable `corepack` run:

```
corepack enable
```

2. Install dependencies

```
yarn
```

3. Run the dev server

```
yarn dev
```

## Running automated tests

`yarn playwright test --ui`