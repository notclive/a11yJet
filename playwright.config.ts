import { defineConfig, devices } from '@playwright/test';

// See https://playwright.dev/docs/test-configuration.
export default defineConfig({
  testDir: './tests',
  // Run tests in files in parallel.
  fullyParallel: true,
  // Fail the build on CI if you accidentally left test.only in the source code.
  forbidOnly: !!process.env.CI,
  // Opt out of parallel tests on CI.
  workers: process.env.CI ? 1 : undefined,
  // Reporter to use. See https://playwright.dev/docs/test-reporters.
  reporter: 'html',

  projects: [
    {
      name: 'chromium',
      use: { ...devices['Desktop Chrome'] },
    },
    {
      name: 'firefox',
      use: { ...devices['Desktop Firefox'] },
    },
    {
      name: 'webkit',
      use: { ...devices['Desktop Safari'] },
    }
  ],

  // Run local dev server before starting the tests.
  webServer: {
    command: 'yarn run build && yarn run preview',
    url: 'http://localhost:4173',
    reuseExistingServer: true
  },
  use: {
    baseURL: 'http://localhost:4173',
    // Collect trace when retrying the failed test. See https://playwright.dev/docs/trace-viewer.
    trace: 'on-first-retry',
  }
})
