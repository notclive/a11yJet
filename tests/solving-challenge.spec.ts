import { test, expect, Page } from '@playwright/test'

test('advancing from landing page', async ({ page }) => {
  // Given
  await page.goto('.')

  // When
  await page.getByText('Show me the source code').click()

  // Then
  await thenUserIsAdvancedToLevel(page, 1)
})

test('solving level 1', async ({ page }) => {
  // Given
  await page.goto('./level/1')

  // When
  await whenTextInEditorIsSelected(page, '<img')
  await page.keyboard.type('<img alt="Great Caribbean offers"')
  await page.getByText('Test my fix').click()
  await expect(page.locator(':root', { hasText: 'Found content from "Caribbean offers" page' })).toBeVisible()
  await page.getByText('Next level').click()

  // Then
  await thenUserIsAdvancedToLevel(page, 2)
})

test('solving level 2', async ({ page }) => {
  // Given
  await page.goto('./level/2')

  // When
  await whenTextInEditorIsSelected(page, 'Where are you flying from?')
  await page.keyboard.type('<label>Where are you flying from?')
  await whenTextInEditorIsSelected(page, '</select>')
  await page.keyboard.type('</select></label>')

  await whenTextInEditorIsSelected(page, 'Departure date\n            <input class="border-2 rounded-md p-2" placeholder="DD/MM/YYYY" />')
  await page.keyboard.type('<label>Departure date\n            <input class="border-2 rounded-md p-2" placeholder="DD/MM/YYYY" /></label>')

  await whenTextInEditorIsSelected(page, 'Return date\n            <input class="border-2 rounded-md p-2" placeholder="DD/MM/YYYY" />')
  await page.keyboard.type('<label>Return date\n            <input class="border-2 rounded-md p-2" placeholder="DD/MM/YYYY" /></label>')

  await page.getByText('Test my fix').click()
  await expect(page.locator(':root', { hasText: 'Found validation message to indicate form was submitted' })).toBeVisible({ timeout: 10 * 1000 })
  await page.getByText('Next level').click()

  // Then
  await thenUserIsAdvancedToLevel(page, 3)
})

test('solving level 3', async ({ page }) => {
  // Given
  await page.goto('./level/3')

  // When
  await whenTextInEditorIsSelected(page, '<p class="my-4 p-2 border text-red-800 bg-red-100 border-red-400 rounded-md">Oh no! There were problems with the values you provided</p>')
  await page.keyboard.press('Delete')

  await whenTextInEditorIsSelected(page, '<input class="border-2 border-fuchsia-800 rounded-md p-2" value="01/03/2023" placeholder="DD/MM/YYYY" />\n            </label>')
  await page.keyboard.type('<input aria-invalid="true" aria-describedby="departure-error" class="border-2 border-fuchsia-800 rounded-md p-2" value="01/03/2023" placeholder="DD/MM/YYYY" />\n        </label>\n        <p id="departure-error" class="my-4 p-2 border text-red-800 bg-red-100 border-red-400 rounded-md">The departure date cannot be in the past</p>')

  await whenTextInEditorIsSelected(page, '<input class="border-2 border-fuchsia-800 rounded-md p-2" value="10/30/2023" placeholder="DD/MM/YYYY" />\n            </label>')
  await page.keyboard.type('<input aria-invalid="true" aria-describedby="return-error" class="border-2 border-fuchsia-800 rounded-md p-2" value="10/30/2023" placeholder="DD/MM/YYYY" />\n            </label>\n        <p id="return-error" class="my-4 p-2 border text-red-800 bg-red-100 border-red-400 rounded-md">The return date is in the wrong format</p>')

  await page.getByText('Test my fix').click()
  await expect(page.locator(':root', { hasText: 'Found content from "Flight selection" page' })).toBeVisible({ timeout: 15 * 1000 })
  await page.getByText('Next level').click()

  // Then
  await thenUserIsAdvancedToLevel(page, 4)
})

test('solving level 4', async ({ page }) => {
  // Given
  await page.goto('./level/4')

  // When
  await whenTextInEditorIsSelected(page, 'background-color: #e5e7eb;')
  await page.keyboard.type('background-color: #f3f4f6;')
  await whenTextInEditorIsSelected(page, 'color: #6b7280;')
  await page.keyboard.type('color: #4b5563;')
  await whenTextInEditorIsSelected(page, 'color: #c026d3;')
  await page.keyboard.type('color: #a21caf;')

  await whenTextInEditorIsSelected(page, 'background-color: #d1d5db;')
  await page.keyboard.type('background-color: #e5e7eb;')
  await whenTextInEditorIsSelected(page, 'color: #6b7280;')
  await page.keyboard.type('color: #4b5563;')
  await whenTextInEditorIsSelected(page, 'background-color: #dc60ef;')
  await page.keyboard.type('background-color: #c026d3;')
  await whenTextInEditorIsSelected(page, 'color: #dc60ef;')
  await page.keyboard.type('color: #a21caf;')
  await whenTextInEditorIsSelected(page, 'color: #9ca3af;')
  await page.keyboard.type('color: #4b5563;')

  await page.getByText('Test my fix').click()
  await expect(page.locator(':root', { hasText: 'Found content from "Confirmation" page' })).toBeVisible()
  await page.getByText('Next level').click()

  // Then
  thenUserIsAdvancedToLevel(page, 5)
})

test('solving level 5', async ({ page }) => {
  // Given
  await page.goto('./level/5')

  // When
  await whenTextInEditorIsSelected(page, '')
  await page.keyboard.type('<a href="#main-content" class="sr-only focus:not-sr-only">\n   Skip to content\n</a>\n')
  await whenTextInEditorIsSelected(page, '<main')
  await page.keyboard.type('<main id="main-content" tabindex="-1"')

  await page.getByText('Test my fix').click()
  await expect(page.locator(':root', { hasText: 'Found content from "Terms and conditions" page' })).toBeVisible({ timeout: 10 * 1000 })
  await page.getByText('Next level').click()

  // Then
  thenUserIsAdvancedToLevel(page, 6)
})

test('solving level 6', async ({ page }) => {
  // Given
  await page.goto('./level/6')

  // When
  await whenTextInEditorIsSelected(page, '<div class="text-xl mb-2">Refund policy</div>')
  await page.keyboard.type('<h3 class="text-xl mb-2">Refund policy</h3>')

  await whenTextInEditorIsSelected(page, '<div class="text-2xl mb-1">Rules of travel</div>')
  await page.keyboard.type('<h2 class="text-xl mb-2">Rules of travel</h2>')

  await whenTextInEditorIsSelected(page, '<div class="text-xl mb-2">Security rules</div>')
  await page.keyboard.type('<h3 class="text-xl mb-2">Security rules</h3>')

  await whenTextInEditorIsSelected(page, '<div class="text-xl mb-2">Prohibited luggage</div>')
  await page.keyboard.type('<div role="heading" aria-level="3" class="text-xl mb-2">Prohibited luggage</div>')

  await page.getByText('Test my fix').click()
  await expect(page.locator(':root', { hasText: 'The "Prohibited luggage" heading was beneath "Rules of travel"' })).toBeVisible()
  await page.getByText('Next level').click()

  // Then
  await expect(page.locator(':root', { hasText: 'You\'ve reached the end of the challenge' })).toBeVisible()
  await expect(page).toHaveURL('./end-of-challenge')
})

test('solving multiple levels', async ({ page }) => {
  // Given
  await page.goto('./level/1')

  // When
  await whenTextInEditorIsSelected(page, '<img')
  await page.keyboard.type('<img alt="Great Caribbean offers"')
  await page.getByText('Test my fix').click()
  await expect(page.locator(':root', { hasText: 'Found content from "Caribbean offers" page' })).toBeVisible()
  await page.getByText('Next level').click()

  await whenTextInEditorIsSelected(page, 'Where are you flying from?')
  await page.keyboard.type('<label>Where are you flying from?')
  await whenTextInEditorIsSelected(page, '</select>')
  await page.keyboard.type('</select></label>')
  await whenTextInEditorIsSelected(page, 'Departure date\n            <input class="border-2 rounded-md p-2" placeholder="DD/MM/YYYY" />')
  await page.keyboard.type('<label>Departure date\n            <input class="border-2 rounded-md p-2" placeholder="DD/MM/YYYY" /></label>')
  await whenTextInEditorIsSelected(page, 'Return date\n            <input class="border-2 rounded-md p-2" placeholder="DD/MM/YYYY" />')
  await page.keyboard.type('<label>Return date\n            <input class="border-2 rounded-md p-2" placeholder="DD/MM/YYYY" /></label>')
  await page.getByText('Test my fix').click()
  await expect(page.locator(':root', { hasText: 'Found validation message to indicate form was submitted' })).toBeVisible({ timeout: 10 * 1000 })
  await page.getByText('Next level').click()

  // Then
  await thenUserIsAdvancedToLevel(page, 3)
})

async function whenTextInEditorIsSelected(page: Page, textToSelect: string) {
  const editor = page.getByRole('textbox')
  const content = await editor.inputValue()
  const selectionStart = content.indexOf(textToSelect)
  expect(selectionStart, `Expected to find "${textToSelect}" in editor`).toBeGreaterThan(-1)
  await editor.focus()
  await editor.evaluate((editor: HTMLTextAreaElement, { selectionStart, selectionEnd }) => {
    editor.selectionStart = selectionStart
    editor.selectionEnd = selectionEnd
  }, {
    selectionStart: selectionStart,
    selectionEnd: selectionStart + textToSelect.length
  })
}

async function thenUserIsAdvancedToLevel(page: Page, level: number) {
  await expect(page.locator(':root', { hasText: `Level ${level}` })).toBeVisible()
  await expect(page).toHaveURL(`./level/${level}`)
}
