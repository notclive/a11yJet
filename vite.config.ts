import react from '@vitejs/plugin-react'
import { Connect, defineConfig } from 'vite'
import { VitePluginRadar } from 'vite-plugin-radar'

// https://vitejs.dev/config/
export default defineConfig({
  // Treat iFrame content as assets, so that they are imported as strings.
  assetsInclude: ['src/jet-site-content/*.html'],
  plugins: [
    react(),
    {
      name: 'redirect-iframe-requests',
      configureServer: redirectIframeRequests,
      configurePreviewServer: redirectIframeRequests
    }
  ]
})

// Matches behaviour of redirect in public/_redirects.
function redirectIframeRequests({ middlewares }: { middlewares: Connect.Server }) {
  middlewares.use((request, _response, next) => {
    if (request.url.startsWith('/iframe/')) {
      request.url = '/iframe.html'
    }
    next()
  })
}